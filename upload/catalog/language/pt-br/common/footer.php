<?php
// Text
$_['text_information']  = 'Informações';
$_['text_service']      = 'Serviços ao cliente';
$_['text_extra']        = 'Outros serviços';
$_['text_contact']      = 'Entre em contato';
$_['text_return']       = 'Solicitar devolução';
$_['text_sitemap']      = 'Mapa do site';
$_['text_manufacturer'] = 'Produtos por marca';
$_['text_voucher']      = 'Comprar vale presentes';
$_['text_affiliate']    = 'Programa de afiliados';
$_['text_special']      = 'Produtos em promoção';
$_['text_account']      = 'Minha conta';
$_['text_order']        = 'Histórico de pedidos';
$_['text_wishlist']     = 'Lista de desejos';
$_['text_newsletter']   = 'Informativo';
$_['text_powered']      = 'Desenvolvido com tecnologia <a href="https://wiki.cyberporto.xyz/Software/Norte">Norte</a><br /> %s &copy; %s';
$_['text_cookie_close']      = 'Close';
$_['text_cookie']            = 'We use cookies to offer you the best experience on our site. By continuing to browse the site, You are agree to use cookies.<br />For obtain more informations please <a href="http://www.aboutcookies.org">Click here</a>';

