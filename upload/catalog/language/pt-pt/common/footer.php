<?php
// Text
$_['text_information']  = 'Informação';
$_['text_service']      = 'Serviços ao Cliente';
$_['text_extra']        = 'Outros Serviços';
$_['text_contact']      = 'Contactos';
$_['text_return']       = 'Devoluções';
$_['text_sitemap']      = 'Mapa do Site';
$_['text_manufacturer'] = 'Produtos por Marca';
$_['text_voucher']      = 'Comprar Vale Oferta';
$_['text_affiliate']    = 'Programa de Afiliados';
$_['text_special']      = 'Produtos em Promoção';
$_['text_account']      = 'A Minha Conta';
$_['text_order']        = 'Histórico de Pedidos';
$_['text_wishlist']     = 'Lista de Favoritos';
$_['text_newsletter']   = 'Newsletter';
$_['text_powered']      = 'Powered By <a href="https://wiki.cyberporto.xyz/Software/Norte">Norte</a><br /> %s &copy; %s';
$_['text_cookie_close']      = 'Close';
$_['text_cookie']       = 'We use cookies to offer you the best experience on our site. By continuing to browse the site, You are agree to use cookies.<br />For obtain more informations please <a href="http://www.aboutcookies.org">Click here</a>';
