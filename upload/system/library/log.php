<?php
/**
 * @package		Norte
 * @author		CodeGroup Team
 * @copyright	Copyright (c) 2021, Norte, Ltd. (https://wiki.cyberporto.xyz/Software/Norte
 * @license		https://opensource.org/licenses/GPL-3.0
 * @link		https://wiki.cyberporto.xyz/Software/Norte
*/

/**
* Log class
*/
class Log {
	private $handle;
	
	/**
	 * Constructor
	 *
	 * @param	string	$filename
 	*/
	public function __construct($filename) {
		$this->handle = fopen(DIR_LOGS . $filename, 'a');
	}
	
	/**
     * 
     *
     * @param	string	$message
     */
	public function write($message) {
		fwrite($this->handle, date('Y-m-d G:i:s') . ' - ' . print_r($message, true) . "\n");
	}
	
	/**
     * 
     *
     */
	public function __destruct() {
		fclose($this->handle);
	}
}