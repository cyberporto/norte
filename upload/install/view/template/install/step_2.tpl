<?php echo $header; ?>
<div class="container">
  <header>
    <div class="row">
      <div class="col-sm-6">
        <h1 class="pull-left">2<small>/4</small></h1>
        <h3><?php echo $heading_title; ?><br/>
          <small><?php echo $text_step_2; ?></small></h3>
      </div>
      <div class="col-sm-6">
        <div id="logo" class="pull-right hidden-xs"><img src="view/image/logo.png" alt="Norte" title="Norte" /></div>
      </div>
    </div>
  </header>
  {% if error_warning %}
  <div class="alert alert-danger alert-dismissible"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  {% endif %}
  <div class="row">
    <div class="col-sm-9">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
        <p><?php echo $text_install_php; ?></p>
        <fieldset>
          <table class="table">
            <thead>
              <tr>
                <td width="35%"><b><?php echo $text_setting; ?></b></td>
                <td width="25%"><b><?php echo $text_current; ?></b></td>
                <td width="25%"><b><?php echo $text_required; ?></b></td>
                <td width="15%" class="text-center"><b><?php echo $text_status; ?></b></td>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td><?php echo $text_version; ?></td>
                <td><?php echo $php_version; ?></td>
                <td>7.3+</td>
                <td class="text-center">{% if php_version >= '7.3' %}
                  <span class="text-success"><i class="fa fa-check-circle"></i></span>
                  {% else %}
                  <span class="text-danger"><i class="fa fa-minus-circle"></i></span>
                  {% endif %}</td>
              </tr>
              <tr>
                <td><?php echo $text_global; ?></td>
                <td>{% if register_globals %}
                  <?php echo $text_on; ?>
                  {% else %}
                  <?php echo $text_off; ?>
                  {% endif %}</td>
                <td><?php echo $text_off; ?></td>
                <td class="text-center">{% if not register_globals %}
                  <span class="text-success"><i class="fa fa-check-circle"></i></span>
                  {% else %}
                  <span class="text-danger"><i class="fa fa-minus-circle"></i></span>
                  {% endif %}</td>
              </tr>
              <tr>
                <td><?php echo $text_magic; ?></td>
                <td>{% if magic_quotes_gpc %}
                  <?php echo $text_on; ?>
                  {% else %}
                  <?php echo $text_off; ?>
                  {% endif %}</td>
                <td><?php echo $text_off; ?></td>
                <td class="text-center">{% if not error_magic_quotes_gpc %}
                  <span class="text-success"><i class="fa fa-check-circle"></i></span>
                  {% else %}
                  <span class="text-danger"><i class="fa fa-minus-circle"></i></span>
                  {% endif %}</td>
              </tr>
              <tr>
                <td><?php echo $text_file_upload; ?></td>
                <td>{% if file_uploads %}
                  <?php echo $text_on; ?>
                  {% else %}
                  <?php echo $text_off; ?>
                  {% endif %}</td>
                <td><?php echo $text_on; ?></td>
                <td class="text-center">{% if file_uploads %}
                  <span class="text-success"><i class="fa fa-check-circle"></i></span>
                  {% else %}
                  <span class="text-danger"><i class="fa fa-minus-circle"></i></span>
                  {% endif %}</td>
              </tr>
              <tr>
                <td><?php echo $text_session; ?></td>
                <td>{% if session_auto_start %}
                  <?php echo $text_on; ?>
                  {% else %}
                  <?php echo $text_off; ?>
                  {% endif %}</td>
                <td><?php echo $text_off; ?></td>
                <td class="text-center">{% if not session_auto_start %}
                  <span class="text-success"><i class="fa fa-check-circle"></i></span>
                  {% else %}
                  <span class="text-danger"><i class="fa fa-minus-circle"></i></span>
                  {% endif %}</td>
              </tr>
            </tbody>
          </table>
        </fieldset>
        <p><?php echo $text_install_extension; ?></p>
        <fieldset>
          <table class="table">
            <thead>
              <tr>
                <td width="35%"><b><?php echo $text_extension; ?></b></td>
                <td width="25%"><b><?php echo $text_current; ?></b></td>
                <td width="25%"><b><?php echo $text_required; ?></b></td>
                <td width="15%" class="text-center"><b><?php echo $text_status; ?></b></td>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td><?php echo $text_db; ?></td>
                <td>{% if db %}
                  <?php echo $text_on; ?>
                  {% else %}
                  <?php echo $text_off; ?>
                  {% endif %}</td>
                <td><?php echo $text_on; ?></td>
                <td class="text-center">{% if db %}
                  <span class="text-success"><i class="fa fa-check-circle"></i></span>
                  {% else %}
                  <span class="text-danger"><i class="fa fa-minus-circle"></i></span>
                  {% endif %}</td>
              </tr>
              <tr>
                <td><?php echo $text_gd; ?></td>
                <td>{% if gd %}
                  <?php echo $text_on; ?>
                  {% else %}
                  <?php echo $text_off; ?>
                  {% endif %}</td>
                <td><?php echo $text_on; ?></td>
                <td class="text-center">{% if gd %}
                  <span class="text-success"><i class="fa fa-check-circle"></i></span>
                  {% else %}
                  <span class="text-danger"><i class="fa fa-minus-circle"></i></span>
                  {% endif %}</td>
              </tr>
              <tr>
                <td><?php echo $text_curl; ?></td>
                <td>{% if curl %}
                  <?php echo $text_on; ?>
                  {% else %}
                  <?php echo $text_off; ?>
                  {% endif %}</td>
                <td><?php echo $text_on; ?></td>
                <td class="text-center">{% if curl %}
                  <span class="text-success"><i class="fa fa-check-circle"></i></span>
                  {% else %}
                  <span class="text-danger"><i class="fa fa-minus-circle"></i></span>
                  {% endif %}</td>
              </tr>
              <tr>
                <td><?php echo $text_openssl; ?></td>
                <td>{% if openssl %}
                  <?php echo $text_on; ?>
                  {% else %}
                  <?php echo $text_off; ?>
                  {% endif %}</td>
                <td><?php echo $text_on; ?></td>
                <td class="text-center">{% if openssl %}
                  <span class="text-success"><i class="fa fa-check-circle"></i></span>
                  {% else %}
                  <span class="text-danger"><i class="fa fa-minus-circle"></i></span>
                  {% endif %}</td>
              </tr>
              <tr>
                <td><?php echo $text_zlib; ?></td>
                <td>{% if zlib %}
                  <?php echo $text_on; ?>
                  {% else %}
                  <?php echo $text_off; ?>
                  {% endif %}</td>
                <td><?php echo $text_on; ?></td>
                <td class="text-center">{% if zlib %}
                  <span class="text-success"><i class="fa fa-check-circle"></i></span>
                  {% else %}
                  <span class="text-danger"><i class="fa fa-minus-circle"></i></span>
                  {% endif %}</td>
              </tr>
              <tr>
                <td><?php echo $text_zip; ?></td>
                <td>{% if zip %}
                  <?php echo $text_on; ?>
                  {% else %}
                  <?php echo $text_off; ?>
                  {% endif %}</td>
                <td><?php echo $text_on; ?></td>
                <td class="text-center">{% if zip %}
                  <span class="text-success"><i class="fa fa-check-circle"></i></span>
                  {% else %}
                  <span class="text-danger"><i class="fa fa-minus-circle"></i></span>
                  {% endif %}</td>
              </tr>
              {% if not iconv %}
              <tr>
                <td><?php echo $text_mbstring; ?></td>
                <td>{% if mbstring %}
                  <?php echo $text_on; ?>
                  {% else %}
                  <?php echo $text_off; ?>
                  {% endif %}</td>
                <td><?php echo $text_on; ?></td>
                <td class="text-center">{% if mbstring %}
                  <span class="text-success"><i class="fa fa-check-circle"></i></span>
                  {% else %}
                  <span class="text-danger"><i class="fa fa-minus-circle"></i></span>
                  {% endif %}</td>
              </tr>
              {% endif %}
            </tbody>
          </table>
        </fieldset>
        <p><?php echo $text_install_file; ?></p>
        <fieldset>
          <table class="table">
            <thead>
              <tr>
                <td><b><?php echo $text_file; ?></b></td>
                <td><b><?php echo $text_status; ?></b></td>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td><?php echo $catalog_config; ?></td>
                <td>{% if not error_catalog_config %}
                  <span class="text-success"><?php echo $text_writable; ?></span>
                  {% else %}
                  <span class="text-danger"><?php echo $error_catalog_config; ?></span>
                  {% endif %}</td>
              </tr>
              <tr>
                <td><?php echo $admin_config; ?></td>
                <td>{% if not error_admin_config %}
                  <span class="text-success"><?php echo $text_writable; ?></span>
                  {% else %}
                  <span class="text-danger"><?php echo $error_admin_config; ?></span>
                  {% endif %}</td>
              </tr>
            </tbody>
          </table>
        </fieldset>
        <p><?php echo $text_install_directory; ?></p>
        <fieldset>
          <table class="table">
            <thead>
              <tr>
                <td align="left"><b><?php echo $text_directory; ?></b></td>
                <td align="left"><b><?php echo $text_status; ?></b></td>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td><?php echo $image; ?>/</td>
                <td>{% if not error_image %}
                  <span class="text-success"><?php echo $text_writable; ?></span>
                  {% else %}
                  <span class="text-danger"><?php echo $error_image; ?></span>
                  {% endif %}</td>
              </tr>
              <tr>
                <td><?php echo $image_cache; ?>/</td>
                <td>{% if not error_image_cache %}
                  <span class="text-success"><?php echo $text_writable; ?></span>
                  {% else %}
                  <span class="text-danger"><?php echo $error_image_cache; ?></span>
                  {% endif %}</td>
              </tr>
              <tr>
                <td><?php echo $image_catalog; ?>/</td>
                <td>{% if not error_image_catalog %}
                  <span class="text-success"><?php echo $text_writable; ?></span>
                  {% else %}
                  <span class="text-danger"><?php echo $error_image_catalog; ?></span>
                  {% endif %}</td>
              </tr>
              <tr>
                <td><?php echo $cache; ?>/</td>
                <td>{% if not error_cache %}
                  <span class="text-success"><?php echo $text_writable; ?></span>
                  {% else %}
                  <span class="text-danger"><?php echo $error_cache; ?></span>
                  {% endif %}</td>
              </tr>
              <tr>
                <td><?php echo $logs; ?>/</td>
                <td>{% if not error_logs %}
                  <span class="text-success"><?php echo $text_writable; ?></span>
                  {% else %}
                  <span class="text-danger"><?php echo $error_logs; ?></span>
                  {% endif %}</td>
              </tr>
              <tr>
                <td><?php echo $download; ?>/</td>
                <td>{% if not error_download %}
                  <span class="text-success"><?php echo $text_writable; ?></span>
                  {% else %}
                  <span class="text-danger"><?php echo $error_download; ?></span>
                  {% endif %}</td>
              </tr>
              <tr>
                <td><?php echo $upload; ?>/</td>
                <td>{% if not error_upload %}
                  <span class="text-success"><?php echo $text_writable; ?></span>
                  {% else %}
                  <span class="text-danger"><?php echo $error_upload; ?></span>
                  {% endif %}</td>
              </tr>
              <tr>
                <td><?php echo $modification; ?>/</td>
                <td>{% if not error_modification %}
                  <span class="text-success"><?php echo $text_writable; ?></span>
                  {% else %}
                  <span class="text-danger"><?php echo $error_modification; ?></span>
                  {% endif %}</td>
              </tr>
            </tbody>
          </table>
        </fieldset>
        <div class="buttons">
          <div class="pull-left"><a href="<?php echo $back; ?>" class="btn btn-default"><?php echo $button_back; ?></a></div>
          <div class="pull-right">
            <input type="submit" value="<?php echo $button_continue; ?>" class="btn btn-primary" />
          </div>
        </div>
      </form>
    </div>
    <div class="col-sm-3"><?php echo $column_left; ?></div>
  </div>
</div>
<?php echo $footer; ?>
